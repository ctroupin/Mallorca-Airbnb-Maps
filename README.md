# Mallorca-Airbnb-Maps

A few maps showing information from the data provided by [Inside Airbnb](http://insideairbnb.com/).

## Heat map

![Heatmap](./figures/airbnbHeatDark.png?raw=true "Heatmap")

## Choropleth map

![Choropleth](./figures/airbnb_choropleth.png?raw=true "Choropleth")

### Data sources

* [National Institute of Statistics](http://www.ine.es/) (INE, Spain).
* [InsideAirbnb](http://insideairbnb.com/), licensed by a  [Creative Commons BY 3.0.](http://creativecommons.org/licenses/by/3.0/)
