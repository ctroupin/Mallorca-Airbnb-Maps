#!/usr/local/bin/python3.6
# coding: utf-8

"""
Convert the CSV data files from http://insideairbnb.com/ to JSON format,
that can be easily ingested by [Leaflet](http://leafletjs.com/).
Data obtained from [Inside Airbnb](http://insideairbnb.com/).
"""

import os
import sys
import csv
import logging
import geojson
import json


class AirbnbCity(object):

    def __init__(self, name, lon=None, lat=None):

        self.name = name

        if lon and lat:
            self.lon = lon
            self.lat = lat
        else:
            self.lon = []
            self.lat = []

    def read_from(self, filename):
        """
        Read coordinates from existing file
        First we identify which columns store the coordinates,
        then we read line by line using the [CSV reader](https://docs.python.org/3/library/csv.html).
        :parameter filename: name of the CSV file
        :type filename: str
        """
        separator = ','
        with open(filename, "r") as f:
            # Read 1st line to get column names
            columnnames = f.readline().split(separator)
            lonindex = columnnames.index("longitude")
            latindex = columnnames.index("latitude")
            logging.debug("Longitude index: {0}".format(lonindex))
            logging.debug("Latitude index: {0}".format(latindex))

            # Use the CSV reader to better take care of the commas inside strings
            # (other solution is to export with ; as separator)
            reader = csv.reader(f)
            for row in reader:
                self.lon.append(row[lonindex])
                self.lat.append(row[latindex])

        # Check how many data points were read
        # (to be compared with `wc -l` to check):
        logging.info("Number of points: {0}".format(len(self.lon)))

    def to_geojson(self, filename):
        """
        Prepare a [multipoint](http://geojson.org/geojson-spec.html#multipoint)
        object that is written to a new file using the [json](https://docs.python.org/3/library/json.html)
        python library.
        :parameter filename: name of the CSV file
        :type filename: str
        """
        multipoint = {"type": "MultiPoint",
                      "coordinates": [[float(llat), float(llon)] for llat, llon in zip(self.lat, self.lon)]
                      }

        with open(os.path.join(filename), 'w') as f:
            f.write("var {0} =".format(self.name))
            json.dump(multipoint, f)


def main():

    if len(sys.argv) > 3:
        logging.error("Too many input arguments")
        logging.info("Usage: AirBNB_csv2json.py inputfile outputfile")
        sys.exit(1)
    elif len(sys.argv) < 3:
        logging.error("Not enough input arguments")
        logging.info("Usage: AirBNB_csv2json.py inputfile outputfile")
        sys.exit(1)
    else:
        datafile = sys.argv[1]
        outputfile = sys.argv[2]

    # datadir = "/home/ctroupin/Data/AirBNB/Brussels/"
    # datafile = os.path.join(datadir, "listings.csv")
    # outputdir = "../data/"
    # outputfile = os.path.join(outputdir, "BruxellesPoints3.js")

    if not(os.path.exists(datafile)):
        logging.error("File {0} doesn't exist".format(datafile))
        sys.exit(1)

    if not(os.path.exists(os.path.dirname(outputfile))):
        logging.error("Directory of file {0} doesn't exist".format(outputfile))
        sys.exit(1)

    city = AirbnbCity()
    city.read_from(datafile)
    city.to_geojson(outputfile)

if __name__ == '__main__':
    main()
